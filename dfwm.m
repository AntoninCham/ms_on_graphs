%% Mumford Shah on graphs: (c) 2019 M. Carrocia, A. Chambolle,
%% D. Slepcev.
%% See paper "MUMFORD - SHAH FUNCTIONALS ON GRAPHS AND THEIR
%% ASYMPTOTICS"

function y = dfwm(x,method);

switch method
 case 0
   % for Mumford-Shah
    y = 1./(1+(pi*pi/4)*(x.^2));
 case 1
   % for TV
    y=1./(2* sqrt(x+0.0001));
 case 2
   % for Dirichlet energy regularizer
    y=ones(length(x),1);
 otherwise
        disp('unknown method');
        y=0;
end


