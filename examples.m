%% Mumford Shah on graphs: (c) 2019 M. Carrocia, A. Chambolle,
%% D. Slepcev.
%% See paper "MUMFORD - SHAH FUNCTIONALS ON GRAPHS AND THEIR
%% ASYMPTOTICS"

% available examples
% 1 - Seattle prices
% 2 - Sacramento prices
% 3 - piecewise linear

example=1;
method=0;
% method : 0 - Mumford-Shah
%          1 - TV 
%          2 - Laplacian 


% computing the minimizers 
switch example
case 1
    M = csvread('kc_house_data2.csv');
    %latitude column 16
    %longitude 17
    %price 1
    %sq ft 4 // 18
    sq = M(:,4);
    M = M(sq~=0,:);
    sq = sq(sq~=0);
    [N ~] = size(M);
    X = M(:,17);
    Y = M(:,16);
    P = M(:,1);
    % exclude indices of outliers
    Indx = find(X<-121.68);
    X=X(Indx);
    Y=Y(Indx);
    P=P(Indx);
    sq=sq(Indx);
    V = [X Y];
    %  is price per sq ft normalized so that largest price/sqft is 1
    u0 = P ./ sq;
    maxp=max(u0);
    f=u0/max(u0);
    u0=f;  
    % set parameters
    lambda = 14; % fidelity parameter,  as in the paper
    signew = 1; % kernel used for weights is eta(s) = exp(-s^2/(2*signew^2))
    epsilon = 0.04; % as in the paper, the kernel width. It also sets the lengthscale that distinguishes between  smooth discrete differences and jumps
    k=15; % k max number of neighbors we consider 
    ddim=2; % dimension of the manifold containing the data; this is needed to recompute the parameters.  
    [u,K] = graphMS(V,f,u0, lambda,epsilon,ddim,signew,k, method);
case 2
    N = 984;
    M = csvread('Sacramentorealestatetransactions2.csv',0,3,[0 3 N-1 6]);
    sq = M(:,1);
    M = M(sq~=0,:);
    sq = sq(sq~=0);
    
    [N ~] = size(M);
    X = M(:,4);
    Y = M(:,3);
    V = [X Y];
    P = M(:,2);  
    u0 = P ./ sq;
    maxp=max(u0);
    f=u0/max(u0);
    u0=f;  
    % set parameters
    lambda = 250; % fidelity parameter
    signew = 1; % kernel used for weights is eta(s) = exp(-s^2/(2*signew^2))
    epsilon = 0.05; 
    k=15; % k max number of neighbors we consider 
    ddim=2; % dimension of the manifold containing the data; this is needed to recompute the parameters. 
    [u,K] = graphMS(V,f,u0,lambda,epsilon,ddim,signew,k, method);
case 3
    N=10000;
    rng(1);
    X=rand(N,1);
    Y=rand(N,1);
    V=[X Y];
    signoise=0.2;
    f=affinefun(X,Y,signoise);
    u0=f;
    % set parameters
    lambda = 162; % fidelity parameter,
    signew = 5; % kernel used for weights is eta(s) = exp(-s^2/(2*signew^2))
    epsilon = 0.0225; % as in the paper, the kernel width. It also sets the lengthscale that distinguishes between  smooth discrete differences and jumps
    k=8; % k max number of neighbors we consider
    ddim=2; % dimension of the manifold containing the data; this is needed to recompute the parameters.
    if method>0
    [u,K] = graphMS(V,f,u0, lambda,epsilon,ddim,signew,k, method);
    else 
    u0 = graphMS(V,f,u0, lambda,epsilon,ddim,signew,k, 2);
    [u,K] = graphMS(V,f,u0, lambda,epsilon,ddim,signew,k, method);
    end;
end

% PLOT images
if ishandle(1); close(1); end;
if ishandle(method+2); close(method+2); end;
switch example
case 1
    dotrad=40;
    Ka = (K<0);
    figure(1)
    colormap(jet);
    quiver(Ka*X,Ka*Y,K*X,K*Y,0,'ShowArrowHead','off');
    hold on;
    scatter(X,Y,dotrad,f,'filled');  caxis([0.05 .75]);
    axis equal;
    hold off;

    figure(2+method)
    colormap(jet);
    quiver(Ka*X,Ka*Y,K*X,K*Y,0,'ShowArrowHead','off');
    hold on;
    scatter(X,Y,dotrad,u,'filled'); caxis([0.05 .75]);
    axis equal;
    hold off;
case 2
    dotrad=40;
    Ka = (K<0);
    figure(1)
    colormap(jet);
    quiver(Ka*X,Ka*Y,K*X,K*Y,0,'ShowArrowHead','off');
    hold on;
    scatter(X,Y,dotrad,f,'filled'); caxis([0 .4]);
    axis equal;
    hold off;
    figure(2+method)
    colormap(jet);
    quiver(Ka*X,Ka*Y,K*X,K*Y,0,'ShowArrowHead','off');
    hold on;
    scatter(X,Y,dotrad,u,'filled'); caxis([0 .4]);
    axis equal;
    hold off;
case 3
    dotrad=50; 
    Ka = (K<0);
    figure(1)
    %colormap(parula);    
    colormap(gray);
   % quiver(Ka*X,Ka*Y,K*X,K*Y,0,'ShowArrowHead','off','LineWidth',2);
    hold on;
    scatter(X,Y,dotrad,f,'filled'); caxis([0 1]);
    axis tight; axis equal; set(gca,'FontSize',20)
    hold off;

    figure(2+method)
    %colormap(parula);
    colormap(gray);
    dd=K*u;
    hold on;
    II=find(abs(dd)>0.075); %MS
   % II=find(abs(dd)>0.12);
   % II=find(abs(dd)>0.08);
    KaX=Ka*X; KaY=Ka*Y; KX=K*X; KY=K*Y;
    quiver(Ka*X,Ka*Y,K*X,K*Y,0,'k','ShowArrowHead','off','Color',[0.4 0.6 1],'LineWidth',2);
    scatter(X,Y,dotrad,u,'filled'); caxis([0 1]);
    quiver(KaX(II),KaY(II),KX(II),KY(II),0,'r','ShowArrowHead','off','LineWidth',6);
    scatter(X,Y,dotrad/2,u,'filled'); caxis([0 1]);
    axis tight; axis equal; set(gca,'FontSize',20);
    hold off;  
end



