%% Mumford Shah on graphs: (c) 2019 M. Carrocia, A. Chambolle,
%% D. Slepcev.
%% See paper "MUMFORD - SHAH FUNCTIONALS ON GRAPHS AND THEIR ASYMPTOTICS"

function [u,K] = graphMS(V,f,u0,lambda,epsilon,ddim,signew,k, method);

%X - input vertices, each rwo is a data point. [N,adim]=size(X)
%f - signal
%lambda - parameter in the functional
%epsilon - length scale as in the functional. For MS differences are
%          considered to be jumps if bigger than sqrt(epsilon)
% ddim - intrinsic dimension of the data. it can be set to zero if not
%       known or if in pure graph setting. Its role here is just that the
%       functional is made to have an asymptotic limit as n->infinity, epsilon->0 
% signew - variance of the Gaussian kernel used to build the graph
% k - maximal degree allowed
% method : 0 - MS
%          1 - TV
%          2 - Laplacian

% OUTPUT
% u - minimizer
% K - adjacency matrix (see below for structure) needed only for visualization

% compute the parameters we used in programing
[N,adim] = size(V);
b=1/epsilon;
a= 1/(epsilon^(ddim+2)*N*lambda);
sigold = signew*epsilon;


% create the graph
tic;
[I,D] = knnsearch(V,V,'k',k+1);

I = [[1:N]' , I];
D = [zeros(N), D];
k = k+1;


GD = exp(-D.^2/(2*sigold*sigold));
K = [];
Eta = sparse(k*N,k*N);

for i=1:k
    % K graph difference operator is an N*k by N matrix
    K = [K ; - sparse(I(:,1),I(:,1),1,N,N) + sparse(I(:,1),I(:,1+i),1,N,N) ];
    % eta - edge weights for the k*N matrix
    Eta(1+(i-1)*N:i*N,1+(i-1)*N:i*N) = spdiags(GD(:,1+i),0,N,N);
end
%% Eta = 1
K = sign(K); %% avoid double entries

nE = size(K,1);

%%% graph matrix : new computation
W = sparse(N,N);
for i=1:k
    W = max(W,sparse(I(:,1),I(:,1+i),GD(:,1+i),N,N));
end
W = max(W,W'); % .* (1-spdiags(ones(N,1),0,N,N));
dia = spdiags(ones(N,1),0,N,N)>0;
W(dia)=0;

e=0;
[ii,jj,ww] = find(W);
updia = (jj>ii);
ii = ii(updia);
jj = jj(updia);
ww = ww(updia);
nE = length(ii);

K = sparse([1:nE],jj,1,nE,N) - sparse([1:nE],ii,1,nE,N);
Eta = spdiags(ww,0,nE,nE);
%%% end graph matrix

% number of iretations
Nit = 300;

% initial condition
u = u0;
% for MS it is sometimes better to use the output of TV or Laplaciam method
% as the initial condition


p = zeros(nE,1);
q = zeros(nE,1);

L = (speye(N,N)+K'*K);
u = u(:);
for i=1:Nit
    L = speye(N,N) + a*K'*spdiags(Eta*dfwm(b*(K*u).^2,method),0,nE,nE)*K;
    u = L\f(:);
end
toc;

