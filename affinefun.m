%% Mumford Shah on graphs: (c) 2019 M. Carrocia, A. Chambolle,
%% D. Slepcev.
%% See paper "MUMFORD - SHAH FUNCTIONALS ON GRAPHS AND THEIR
%% ASYMPTOTICS"

function f = affinefun(x,y,sig)

rng(2);
[N,M]=size(x);
f=0.7*(x+0.1);
I=find(((x>0.25) & (y<0.6+x/4)) & (y<5.5-6*x));
f(I)= 1.2-1.2*x(I);
J=find((2*(x-0.1) +y-0.8).^2 + (x-0.1-2*(y-0.8)).^2/4 <0.4);
f(J) = 0.8-0.8*(2*(x(J)-0.1) +y(J) -0.5);
f=max(f,0);
f=min(f,1);
if sig>0
    noise = normrnd(0,sig,N,M);
    f=f+noise;
end;

